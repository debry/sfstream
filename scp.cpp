// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;


int main(int argc, char *argv[])
{
  TRY;

  if (argc < 3)
    throw string("usage: scp "
                 "[[logname@]hostname:[port:]][/]path/to/input/file "
                 "[[logname@]hostname:[port:]][[/]path/to/output/file]");

  isfstream<scp> fin(argv[1]);
  if (! fin.good())
    throw Error("Could not open input file \"" + string(argv[1]) + "\".");

  // Get file size and permission, this later in octanal format.
  const size_t file_size = fin.rdbuf()->file()->size();
  const mode_t file_perm = fin.rdbuf()->file()->perm<oct>();

  vector<char> buffer(file_size, '\0');

  fin.read(buffer.data(), file_size * sizeof(char));
  fin.close();

  // SCP always truncate output file.
  osfstream<scp> fout(argv[2], ios_base::out, option::perm = file_perm, option::size = file_size);
  if (! fout.good())
    throw Error("Could not open output file \"" + string(argv[2]) + "\".");

  fout.write(buffer.data(), file_size * sizeof(char));
  fout.close();

  END;

  return 0;
}
