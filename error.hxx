// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_ERROR_HXX


namespace std
{
  class Error: public std::exception
  {
  private:
    string message_;

  public:
    Error(string message, char *errmsg = NULL) throw();

    virtual ~Error() throw();

    virtual string What();
    void CoutWhat();
  };


  /*!< Errors due to libssh2.*/
  namespace libssh2
  {
    class Error: public std::exception
    {
    private:
      string message_;

    public:
      Error(string message, LIBSSH2_SESSION *session = NULL) throw();
 
      virtual ~Error() throw();
    
      virtual string What();
      void CoutWhat();
    };
  }
}

#define SFSTREAM_FILE_ERROR_HXX
#endif
