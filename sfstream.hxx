// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SFSTREAM_HXX

#include "sfstreamHeader.hxx"

namespace std
{
  template<typename T>
  string to_str(const T& value)
  {
    ostringstream oss;
    oss << value;
    return oss.str();
  }

  template<typename T>
  T convert(const char* s)
  {
    T value;
    istringstream iss(s);
    iss >> value;
    return value;
  }
}

#include "error.cxx"
#include "session.cxx"
#include "basic_file.cxx"
#include "sfilebuf.cxx"
#include "isfstream.cxx"
#include "osfstream.cxx"

#define SFSTREAM_FILE_SFSTREAM_HXX
#endif
