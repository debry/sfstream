// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SESSION_HXX

#define SSH_PORT  22
#define HOSTNAME  "localhost"
#define LIBSSH2_DEBUG_LEVEL_0 LIBSSH2_TRACE_SCP | LIBSSH2_TRACE_SFTP | LIBSSH2_TRACE_AUTH | LIBSSH2_TRACE_PUBLICKEY
#define LIBSSH2_DEBUG_LEVEL_1 LIBSSH2_DEBUG_LEVEL_0 | LIBSSH2_TRACE_KEX | LIBSSH2_TRACE_CONN
#define LIBSSH2_DEBUG_LEVEL_2 LIBSSH2_DEBUG_LEVEL_1 | LIBSSH2_TRACE_TRANS | LIBSSH2_TRACE_SOCKET
#define LIBSSH2_DEBUG_LEVEL_3 LIBSSH2_DEBUG_LEVEL_2 | LIBSSH2_TRACE_ERROR


namespace std
{
  int socket_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);

  namespace session
  {
    template<class libssh2_session_type>
    class base
    {
    public:
      typedef libssh2_session_type libssh2_type;

    protected:

      /*!< Socket id.*/
      int sock_;

      /*!< Base session.*/
      LIBSSH2_SESSION *session_base_;

      /*!< Specialized session pointer.*/
      libssh2_session_type *session_ptr_;

      /*!< Authentification.*/
      void authenticate(const string &logname);

    public:

      /*!< Constructors.*/
      base();

      /*!< Destructor.*/
      virtual ~base();

      /*!< Connect.*/
      virtual void connect(string address, int debug_level = -1);
      virtual void connect(const string logname, const string hostname,
                           const int port = SSH_PORT, int debug_level = -1);

      /*!< Disconnect.*/
      virtual void disconnect(const string description = "");

      /*!< Set trace.*/
      void trace(int bitmask = 0);
    };


    class scp : public base<LIBSSH2_SESSION>
    {
    public:

      /*!< Constructors.*/
      scp();

      /*!< Destructor.*/
      ~scp();

      /*!< Connect.*/
      virtual void connect(const string logname, const string hostname,
                           const int port = SSH_PORT, int debug_level = -1);

      /*!< Disconnect.*/
      void disconnect(const string description = "");
    };


    class sftp : public base<LIBSSH2_SFTP>
    {
    public:

      /*!< Constructors.*/
      sftp();

      /*!< Destructor.*/
      ~sftp();

      /*!< Connect.*/
      virtual void connect(const string logname, const string hostname,
                           const int port = SSH_PORT, int debug_level = -1);

      /*!< Disconnect.*/
      void disconnect(const string description = "");
    };
  }
}

#define SFSTREAM_FILE_SESSION_HXX
#endif
