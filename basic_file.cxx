// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_BASIC_FILE_CXX

namespace std
{
  namespace basic_file
  {
    //
    // Base file.
    //


    template<class libssh2_handle_type>
    base<libssh2_handle_type>::base()
      : libssh2_handle_ptr_(NULL), mode_(ios_base::openmode(0)),
        file_size_(0), file_pos_(0), file_perm_(0)
    {
      return;
    }


    // Destructor.
    template<class libssh2_handle_type>
    base<libssh2_handle_type>::~base()
    {
      return;
    }


    // Is file opened ?
    template<class libssh2_handle_type>
    bool base<libssh2_handle_type>::is_open() const
    {
      return libssh2_handle_ptr_ != NULL;
    }


    // Get characters.
    template<class libssh2_handle_type>
    size_t base<libssh2_handle_type>::xsgetn(char *s, size_t n)
    {
      if (libssh2_handle_ptr_ != NULL)
        {
          if (n > (file_size_ - file_pos_))
            n = file_size_ - file_pos_;

          size_t nleft(n);

          while (nleft > 0)
            {
              size_t rc;
              if ((rc = this->read(s, nleft)) < 0)
                break;

              nleft -= rc;
              s += rc;
              file_pos_ += rc;
            }

          n -= nleft;
        }
      else
        n = 0;

      return n;
    }


    // Put characters.
    template<class libssh2_handle_type>
    size_t base<libssh2_handle_type>::xsputn(const char *s, size_t n)
    {
      if (libssh2_handle_ptr_ != NULL)
        {
          size_t nleft(n);

          while (nleft > 0)
            {
              size_t rc;
              if ((rc = this->write(s, nleft)) < 0)
                break;

              nleft -= rc;
              s += rc;
              file_pos_ += rc;

              // The file size may be incremented or not
              // depending on the ssh2 protocol (scp or sftp).
            }

          n -= nleft;
        }
      else
        n = 0;

      return n;      
    }


    // Get and set position.
    template<class libssh2_handle_type>
    size_t base<libssh2_handle_type>::seekoff(size_t off, ios_base::seekdir way)
    {
      return size_t(-1);
    }


    // File size.
    template<class libssh2_handle_type>
    size_t base<libssh2_handle_type>::size() const
    {
      return file_size_;
    }


    // File permissions.
    template<class libssh2_handle_type> template<ios_base& (*fmt)(ios_base&)>
    mode_t base<libssh2_handle_type>::perm() const
    {
      mode_t file_perm(0);
      stringstream ss;
      ss << fmt << file_perm_;
      ss >> dec >> file_perm;
      return file_perm;
    }


    //
    // SCP based file.
    //


    // Constructor.
    scp::scp() : base<LIBSSH2_CHANNEL>()
    {
      return;
    }

    // Destructor.
    scp::~scp()
    {
      //close();
      return;
    }


    // Open.
    scp* scp::open(const char *cpath, ios_base::openmode mode, mode_t permission,
                   size_t size, libssh2_session_type *session)
    {
      // Return if no valid session or channel already opened.
      if (session == NULL || this->libssh2_handle_ptr_ != NULL)
        return NULL;

      //
      // Request a file via SCP.
      //

      // Through SCP, file cannot be opened in read and write modes.
      if (mode & ios_base::in && mode & ios_base::out)
        return reinterpret_cast<scp*>(this->libssh2_handle_ptr_ = NULL);

      if (mode & ios_base::in)
        {
          struct stat fileinfo;

          if (! (this->libssh2_handle_ptr_ = libssh2_scp_recv(session, cpath, &fileinfo)))
            return reinterpret_cast<scp*>(this->libssh2_handle_ptr_ = NULL);
          this->file_size_ = fileinfo.st_size;
          this->file_perm_ = fileinfo.st_mode;
        }
      else if (mode & ios_base::out)
        {
          // Writing through SCP requires the file size to be known ahead of time.
          if (size < 0)
            return reinterpret_cast<scp*>(this->libssh2_handle_ptr_ = NULL);

          if (! (this->libssh2_handle_ptr_ = libssh2_scp_send(session, cpath, permission, size)))
            return reinterpret_cast<scp*>(this->libssh2_handle_ptr_ = NULL);

          this->file_size_ = size;
          this->file_perm_ = permission;
        }
      else
        return reinterpret_cast<scp*>(this->libssh2_handle_ptr_ = NULL);

      this->file_pos_ = 0;
      this->mode_ = mode;

      return this;
    }


    // Close.
    scp* scp::close()
    {
      scp *ret = this;

      if (this->libssh2_handle_ptr_ != NULL)
        {
          if (mode_ & ios_base::out)
            {
              if (libssh2_channel_send_eof(this->libssh2_handle_ptr_)) ret = NULL;
              if (libssh2_channel_wait_eof(this->libssh2_handle_ptr_)) ret = NULL;
              if (libssh2_channel_close(this->libssh2_handle_ptr_)) ret = NULL;
              if (libssh2_channel_wait_closed(this->libssh2_handle_ptr_)) ret = NULL;
            }

          if (libssh2_channel_free(this->libssh2_handle_ptr_))
            ret = NULL;
          else
            this->libssh2_handle_ptr_ = NULL;

          this->file_size_ = 0;
          this->file_perm_ = 0;
        }
      else
        ret = NULL;

      return ret;
    }


    // Read.
    inline size_t scp::read(char *s, size_t n)
    {
      return libssh2_channel_read(this->libssh2_handle_ptr_, s, n);
    }


    // Write.
    inline size_t scp::write(const char *s, size_t n)
    {
      return libssh2_channel_write(this->libssh2_handle_ptr_, s, n);
      // Through SCP file size is fixed.
    }


    // Get and set position.
    size_t scp::seekoff(size_t off, ios_base::seekdir way)
    {
      return this->file_pos_ + off;
    }


    // Fill file with given character.
    size_t scp::fill(const char c)
    {
      size_t n = this->file_size_ - this->file_pos_;

      if (n > 0)
        {
          vector<char> fill(n, c);
          n = write(fill.data(), n);
          this->file_pos_ += n;
        }

      return n;
    }


    //
    // FTP based file.
    //


    // Constructor.
    sftp::sftp() : base<LIBSSH2_SFTP_HANDLE>()
    {
      return;
    }


    // Destructor.
    sftp::~sftp()
    {
      close();
      return;
    }


    // Open.
    sftp* sftp::open(const char *cpath, ios_base::openmode mode, mode_t permission,
                     size_t size, libssh2_session_type *session)
    {
      // Return if no valid session or channel already opened.
      if (session == NULL || this->libssh2_handle_ptr_ != NULL)
        return NULL;

      //
      // Request a file via SFTP.
      //

      unsigned long libssh2_flags(0);

      if (mode & ios_base::in)    libssh2_flags = libssh2_flags | LIBSSH2_FXF_READ;
      // File created if does not exist.
      if (mode & ios_base::out)   libssh2_flags = libssh2_flags | (LIBSSH2_FXF_WRITE | LIBSSH2_FXF_CREAT);
      if (mode & ios_base::trunc) libssh2_flags = libssh2_flags | LIBSSH2_FXF_TRUNC;
      if (mode & ios_base::app)   libssh2_flags = libssh2_flags | LIBSSH2_FXF_APPEND;

      long libssh2_mode(0);
      if (permission & S_IRUSR) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IRUSR;
      if (permission & S_IWUSR) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IWUSR;
      if (permission & S_IXUSR) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IXUSR;
      if (permission & S_IRGRP) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IRGRP;
      if (permission & S_IWGRP) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IWGRP;
      if (permission & S_IXGRP) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IXGRP;
      if (permission & S_IROTH) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IROTH;
      if (permission & S_IWOTH) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IWOTH;
      if (permission & S_IXOTH) libssh2_mode = libssh2_mode | LIBSSH2_SFTP_S_IXOTH;

      if (! (this->libssh2_handle_ptr_ = libssh2_sftp_open(session, cpath, libssh2_flags, libssh2_mode)))
        return reinterpret_cast<sftp*>(this->libssh2_handle_ptr_ = NULL);

      // File attributes.
      LIBSSH2_SFTP_ATTRIBUTES file_attribute;
      if(libssh2_sftp_fstat(this->libssh2_handle_ptr_, &file_attribute) < 0)
        {
          this->file_size_ = 0;
          this->file_perm_ = permission;
        }
      else
        {
          this->file_size_ = size_t(file_attribute.filesize);
          this->file_perm_ = mode_t(file_attribute.permissions);
        }

      // Workaround as libssh2 append mode does not seem to work.
      if ((mode & ios_base::app) && this->file_size_ > 0)
        this->file_pos_ = seekoff(this->file_size_, ios_base::beg);
      else
        this->file_pos_ = size_t(0);

      this->mode_ = mode;

      return this;
    }


    // Close.
    sftp* sftp::close()
    {
      sftp *ret = this;

      if (this->libssh2_handle_ptr_ != NULL)
        {
          if (libssh2_sftp_close(this->libssh2_handle_ptr_))
            ret = NULL;
          else
            this->libssh2_handle_ptr_ = NULL;

          this->file_size_ = 0;
          this->file_perm_ = 0;

          return this;
        }
      else
        ret = NULL;

      return ret;
    }


    // Read.
    inline size_t sftp::read(char *s, size_t n)
    {
      return libssh2_sftp_read(this->libssh2_handle_ptr_, s, n);
    }


    // Write.
    inline size_t sftp::write(const char *s, size_t n)
    {
      n = libssh2_sftp_write(this->libssh2_handle_ptr_, s, n);
      if (n > 0) this->file_size_ += n;
      return n;
    }


    // Get and set position.
    size_t sftp::seekoff(size_t off, ios_base::seekdir way)
    {
      // Assume here that file_size_ is valid.
      if (way == ios_base::cur)
        off += this->file_pos_;
      else if (way == ios_base::end)
        off += this->file_size_;

      libssh2_sftp_seek64(libssh2_handle_ptr_, libssh2_uint64_t(off)); 
      this->file_pos_ = size_t(libssh2_sftp_tell64(libssh2_handle_ptr_));

      return this->file_pos_;
    }
  }
}

#define SFSTREAM_FILE_BASIC_FILE_CXX
#endif
