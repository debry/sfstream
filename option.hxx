// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_OPTION_HXX

namespace option
{
#include "np.hxx"

  class size : public base
  {
  private:
    size_t value_, default_;

  public:

    size() : base("size", &value_), value_(size_t(-1)),
             default_(size_t(-1)) {return;}

    void reset() {value_ = default_;}

    size* operator= (size_t value)
    {
      value_ = value;
      return this;
    }
  } size;


  class perm : public base
  {
  private:
    mode_t value_, default_;

  public:

    perm() : base("perm", &value_), value_(mode_t(644)),
             default_(mode_t(644)) {return;}

    void reset() {value_ = default_;}

    perm* operator= (mode_t value)
    {
      stringstream ss;
      ss << dec << value;
      ss >> oct >> value_;
      return this;
    }
  } perm;

  NAMED_PARAMETER(libssh2_debug_level, int, 0);
}


#define SFSTREAM_FILE_OPTION_HXX
#endif
