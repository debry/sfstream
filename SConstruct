# Copyright (C) 2015 Edouard Debry
# Author(s) : Edouard Debry
#
# This file is part of sfstream.
#
# sfstream is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sfstream is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

import os, glob, distutils.sysconfig

#
# Some default paths.
#

sfstream_path = os.environ["HOME"] + "/code/sfstream"

include_path_list = [distutils.sysconfig.get_python_inc(),
                     sfstream_path,
                     os.path.join(sfstream_path, "include")]

library_list = ["ssh2"]

#
# Some default arguments.
#

if not ARGUMENTS.has_key("line"):
    ARGUMENTS["line"] = "no"

if not ARGUMENTS.has_key("debug"):
    ARGUMENTS["debug"] = "no"

#
# Build environment.
#

env = Environment(ENV = os.environ)
env.Replace(CONFIGURELOG = "./.scons.log")
env.Replace(CDEFINES = "_REENTRANT")
env.Replace(CPPDEFINES = "")
env.Replace(CCFLAGS = "-O2")
env.Append(CPPFLAGS = "-std=gnu++0x")

if ARGUMENTS["debug"] == "yes":
    env.Replace(CCFLAGS = "-g")

if ARGUMENTS.has_key("proto"):
    env.Append(CPPDEFINES = "PROTOCOL=%s" % ARGUMENTS["proto"])

env.Append(CPPPATH = include_path_list)

if ARGUMENTS["line"] == "no":
    env.Replace(CXXCOMSTR = "[C++] $SOURCE")
    env.Replace(LINKCOMSTR = "[Linking] $TARGET")
    env.Replace(SHCXXCOMSTR = "[Shared C++] $SOURCE")
    env.Replace(SHLINKCOMSTR = "[Shared linking] $TARGET")

if os.environ.has_key("LD_LIBRARY_PATH"):
    env.Append(LIBPATH = os.environ["LD_LIBRARY_PATH"].split(":"))
if os.environ.has_key("LIBRARY_PATH"):
    env.Append(LIBPATH = os.environ["LIBRARY_PATH"].split(":"))
if os.environ.has_key("CPATH"):
    env.Append(CPPPATH = os.environ["CPATH"].split(":"))
if os.environ.has_key("CPLUS_INCLUDE_PATH"):
    env.Append(CPPPATH = os.environ["CPLUS_INCLUDE_PATH"].split(":"))

conf = Configure(env)

for library in library_list:
    if conf.CheckLib(library):
        env.Append(LINKFLAGS = "-l" + library)

target_list = glob.glob("*.cpp")

for filename in target_list:
    env.Program(filename[:-4], [filename])
