// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_PROTOCOL_HXX

namespace std
{
  namespace protocol
  {
    class scp
    {
    public:
      typedef basic_file::scp                       file_type;
      typedef basic_file::scp::libssh2_session_type libssh2_session_type;
      typedef session::scp                          session_type;
    };


    class sftp
    {
    public:
      typedef basic_file::sftp                       file_type;
      typedef basic_file::sftp::libssh2_session_type libssh2_session_type;
      typedef session::sftp                          session_type;
    };
  }
}

#define SFSTREAM_FILE_PROTOCOL_HXX
#endif
