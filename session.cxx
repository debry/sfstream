// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SESSION_CXX

namespace std
{
  int socket_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
  {
    return connect(sockfd, addr, addrlen);
  }

  namespace session
  {
    // Authenticate.
    template<class libssh2_session_type>
    void base<libssh2_session_type>::authenticate(const string &logname)
    {
      // At this point we have not yet authenticated.  The first thing to do
      // is check the hostkey's fingerprint against our known hosts Your app
      // may have it hard coded, may go to a file, may present it to the
      // user, that's your call.
      const char *fingerprint = libssh2_hostkey_hash(session_base_, LIBSSH2_HOSTKEY_HASH_SHA1);

      // Connect to the ssh-agent.
      LIBSSH2_AGENT *agent = libssh2_agent_init(session_base_);

      if (!agent)
        throw libssh2::Error("Failed to initialize ssh-agent : ", session_base_);

      if (libssh2_agent_connect(agent))
        throw libssh2::Error("Failed to connect to ssh-agent : ", session_base_);

      if (libssh2_agent_list_identities(agent))
        throw libssh2::Error("Failed to request identities to ssh-agent : ", session_base_);

      struct libssh2_agent_publickey *identity, *prev_identity = NULL;

      int rc;
      while ((rc = libssh2_agent_get_identity(agent, &identity, prev_identity)) != 1)
        {
          if (rc < 0)
            throw libssh2::Error("Failed to obtain identity from ssh-agent : ", session_base_);

          rc = libssh2_agent_userauth(agent, logname.c_str(), identity);

          // If we succeed.
          if (! rc) break;

          prev_identity = identity;
        }

      if (rc) throw libssh2::Error("Could not authenticate : ", session_base_);

      libssh2_agent_free(agent);
    }


    // Constructors.
    template<class libssh2_session_type>
    base<libssh2_session_type>::base() : sock_(0), session_base_(NULL), session_ptr_(NULL)
    {
      return;
    }


    // Destructor.
    template<class libssh2_session_type>
    base<libssh2_session_type>::~base()
    {
      disconnect();
      return;
    }


    // Connect.
    template<class libssh2_session_type>
    void base<libssh2_session_type>::connect(string address, int debug_level)
    {
      // Default logname, hostname and port.
      string logname = getenv("LOGNAME") != NULL ? string(getenv("LOGNAME")) : "";
      string hostname = HOSTNAME;
      int port = SSH_PORT;

      // Parse input string for logname, hostname and port.
      size_t pos;

      if ((pos = address.find('@')) != string::npos)
        {
          logname = address.substr(0, pos);
          address = address.substr(pos + 1);
        }

      if (! address.empty())
        {
          if ((pos = address.find(':')) != string::npos)
            {
              hostname = address.substr(0, pos);
              std::istringstream iss(address.substr(pos + 1));
              iss >> port;
            }
          else
            hostname = address;
        }

      this->connect(logname, hostname, port, debug_level);
    }


    template<class libssh2_session_type>
    void base<libssh2_session_type>::connect(const string logname, const string hostname,
                                             const int port, int debug_level)
    {
      // If session not null, probably already opened.
      if (session_base_ != NULL)
        return;

      // Init libssh2 subsystem.
      int rc;
      if ((rc = libssh2_init(0)) != 0)
        throw libssh2::Error("Initialization failed with error code " + to_str(rc) + ".");

      // Init socket layer.
      struct addrinfo hints, *addr = NULL , *p = NULL;

      bzero(&hints, sizeof hints);
      hints.ai_family = AF_INET;
      hints.ai_socktype = SOCK_STREAM;

      ostringstream service;
      service << port;

      getaddrinfo(hostname.c_str(), service.str().c_str(), &hints, &addr);

      // Loop through all the results and connect to the first we can.
      for(p = addr; p != NULL; p = p->ai_next)
        {
          if ((sock_ = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0)
            continue;

          if (socket_connect(sock_, p->ai_addr, p->ai_addrlen) < 0)
            {
              shutdown(sock_, SHUT_RDWR);
              continue;
            }

          // if we get here, we must have connected successfully
          break;
        }

      // Looped off the end of the list with no connection.
      if (p == NULL)
        throw Error("Failed to connect to \"" + hostname +
                    "\" on port " + to_str(port) + " : ", strerror(errno));

      freeaddrinfo(addr);
 
      // Now create SSH session ...
      session_base_ = libssh2_session_init();
      if(session_base_ == NULL)
        throw libssh2::Error("Failed to create SSH session.");

      // Init trace.
      if (getenv("LIBSSH2_DEBUG_LEVEL") != NULL)
        debug_level = convert<int>(getenv("LIBSSH2_DEBUG_LEVEL"));

      if (debug_level == 0)
        trace(LIBSSH2_DEBUG_LEVEL_0);
      else if (debug_level == 1)
        trace(LIBSSH2_DEBUG_LEVEL_1);
      else if (debug_level == 2)
        trace(LIBSSH2_DEBUG_LEVEL_2);
      else if (debug_level == 3)
        trace(LIBSSH2_DEBUG_LEVEL_3);
      else
        trace(0);

      // Make all calls blocking, like regular files.
      libssh2_session_set_blocking(session_base_, 1); 

      // ... and start it up. This will trade welcome banners,
      // exchange keys, setup crypto, compression and MAC layers.
      if ((rc = libssh2_session_handshake(session_base_, sock_)))
        throw libssh2::Error("Failed to establish SSH session :", session_base_);

      // Now authenticate with given logname.
      authenticate(logname);
    }


    // Disconnect.
    template<class libssh2_session_type>
    void base<libssh2_session_type>::disconnect(const string description)
    {
      if (session_base_ != NULL)
        {
          libssh2_session_disconnect(session_base_, description.c_str());
          libssh2_session_free(session_base_);
          libssh2_exit();

          session_base_ = NULL;
        }

      shutdown(sock_, SHUT_RDWR);
    }


    // Set trace.
    template<class libssh2_session_type>
    void base<libssh2_session_type>::trace(int bitmask)
    {
      if (session_base_ != NULL)
        libssh2_trace(session_base_, bitmask);
    }


    // Constructors.
    scp::scp() : base<LIBSSH2_SESSION>()
    {
      return;
    }


    // Destructor.
    scp::~scp()
    {
      scp::disconnect();
      return;
    }


    // Connect.
    void scp::connect(const string logname, const string hostname, const int port, int debug_level)
    {
      base<LIBSSH2_SESSION>::connect(logname, hostname, port, debug_level);
      this->session_ptr_ = this->session_base_;
    }


    // Disconnect.
    void scp::disconnect(const string description)
    {
      this->session_ptr_ = NULL;
      base<LIBSSH2_SESSION>::disconnect(description);
    }


    // Constructors.
    sftp::sftp() : base<LIBSSH2_SFTP>()
    {
      return;
    }


    // Destructor.
    sftp::~sftp()
    {
      sftp::disconnect();
      return;
    }


    // Connect.
    void sftp::connect(const string logname, const string hostname, const int port, int debug_level)
    {
      base<LIBSSH2_SFTP>::connect(logname, hostname, port, debug_level);

      if (this->session_ptr_ == NULL)
        if (! (this->session_ptr_ = libssh2_sftp_init(this->session_base_)))
          throw libssh2::Error("Failed to establish SFTP session :", this->session_base_); 
    }


    // Disconnect.
    void sftp::disconnect(const string description)
    {
      libssh2_sftp_shutdown(this->session_ptr_);
      this->session_ptr_ = NULL;

      base<LIBSSH2_SFTP>::disconnect(description);
    }
  }
}

#define SFSTREAM_FILE_SESSION_CXX
#endif
