// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

#ifndef PROTOCOL
#define PROTOCOL scp
#endif

int main(int argc, char *argv[])
{
  TRY;

  if (argc == 1)
    throw string("Need one argument [[logname@]hostname:[port:]][/]path/to/file");

  isfstream<PROTOCOL> fin(argv[1]);

  if (! fin.good())
    throw Error("Could not open file.");

  const int n = fin.rdbuf()->file()->size() / sizeof(double);
  DISP(n);

  vector<double> data(n, double(0));

  fin.read(reinterpret_cast<char*>(data.data()), n * sizeof(double));

  for (int i = 0; i < n; ++i)
    cout << " " << setprecision(8) << data[i];
  cout << endl;

  fin.close();

  END;

  return 0;
}
