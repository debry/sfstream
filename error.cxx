// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_ERROR_CXX

#include "error.hxx"

namespace std
{
  Error::Error(string message, char *errmsg) throw() : message_(message)
  {
    if (errmsg != NULL)
      message_ += errmsg;
  }


  Error::~Error() throw()
  {
  }


  string Error::What()
  {
    return "Error : " + message_;
  }


  void Error::CoutWhat()
  {
    cout << this->What() << endl;
  }


  // Errors due to libssh2.
  namespace libssh2
  {
    Error::Error(string message, LIBSSH2_SESSION *session) throw() : message_(message)
    {
      if (session != NULL)
        {
          char *errmsg = NULL;
          libssh2_session_last_error(session, &errmsg, NULL, 0);
          if (errmsg != NULL)
            message_ += errmsg;
        }
    }


    Error::~Error() throw()
    {
    }


    string Error::What()
    {
      return "[libssh2] Error : " + message_;
    }


    void Error::CoutWhat()
    {
      cout << this->What() << endl;
    }
  }
}

#define SFSTREAM_FILE_ERROR_CXX
#endif
