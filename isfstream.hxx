// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_ISFSTREAM_HXX

namespace std
{
  template<class protocol_type = protocol::sftp>
  class isfstream : public std::istream
  {
  private:

    /*< Buffer.*/
    sfilebuf<protocol_type> buffer_;

  public:

    /*!< Constructors.*/
    isfstream();
    isfstream(const char *cpath, ios_base::openmode mode = ios_base::in, option::base *opt1 = NULL);

    /*!< Destructor.*/
    ~isfstream();

    /*!< Get methods.*/
    sfilebuf<protocol_type>* rdbuf();

    /*!< Open.*/
    void open(const char *cpath, ios_base::openmode mode = ios_base::in, option::base *opt1 = NULL);

    /*!< Close.*/
    void close();
  };
}

#define SFSTREAM_FILE_ISFSTREAM_HXX
#endif
