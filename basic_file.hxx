// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_BASIC_FILE_HXX

namespace std
{
  namespace basic_file
  {
    template<class libssh2_handle_type>
    class base
    {
    protected:

      /*!< Libssh2 handle pointer.*/
      libssh2_handle_type *libssh2_handle_ptr_;

      /*!< Open mode.*/
      ios_base::openmode mode_;

      /*!< File permissions.*/
      mode_t file_perm_;

      /*!< File size.*/
      size_t file_size_;

      /*!< Position in file.*/
      size_t file_pos_;

    public:

      /*!< Constructor.*/
      base();

      /*!< Destructor.*/
      virtual ~base();

      /*!< If file open ?*/
      bool is_open() const;

      /*!< Read.*/
      virtual size_t read(char *s, size_t n) = 0;

      /*!< Write.*/
      virtual size_t write(const char *s, size_t n) = 0;

      /*!< Get characters.*/
      size_t xsgetn(char *s, size_t n);

      /*!< Put characters.*/
      size_t xsputn(const char *s, size_t n);

      /*!< Get and set position.*/
      virtual size_t seekoff(size_t off, ios_base::seekdir way);

      /*!< File size.*/
      size_t size() const;

      /*!< File permissions.*/
      template<ios_base& (*fmt)(ios_base&)>
      mode_t perm() const;
    };


    class scp : public base<LIBSSH2_CHANNEL>
    {
    public:

      typedef LIBSSH2_SESSION libssh2_session_type;

    public:

      /*!< Constructor.*/
      scp();

      /*!< Destructor.*/
      ~scp();

      /*!< Open.*/
      scp* open(const char *cpath, ios_base::openmode mode, mode_t permission,
                size_t size, libssh2_session_type *session);

      /*!< Close.*/
      scp* close();

      /*!< Read.*/
      size_t read(char *s, size_t n);

      /*!< Write.*/
      size_t write(const char *s, size_t n);

      /*!< Get and set position.*/
      size_t seekoff(size_t off, ios_base::seekdir way);

      /*!< Fill file with given character.*/
      size_t fill(const char c);
    };


    class sftp : public base<LIBSSH2_SFTP_HANDLE>
    {
    public:

      typedef LIBSSH2_SFTP libssh2_session_type;

    public:

      /*!< Constructor.*/
      sftp();

      /*!< Destructor.*/
      ~sftp();

      /*!< Open.*/
      sftp* open(const char *cpath, ios_base::openmode mode, mode_t permission,
                 size_t size, libssh2_session_type *session);

      /*!< Close.*/
      sftp* close();

      /*!< Read.*/
      size_t read(char *s, size_t n);

      /*!< Write.*/
      size_t write(const char *s, size_t n);

      /*!< Get and set position.*/
      size_t seekoff(size_t off, ios_base::seekdir way);
    };
  }
}

#define SFSTREAM_FILE_BASIC_FILE_HXX
#endif
