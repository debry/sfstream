// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

int main(int argc, char *argv[])
{
  TRY;

  if (argc < 3)
    throw string("Need two arguments /path/to/local/file [logname@]hostname:[port:][/]path/to/file [mode] [pos]");

  ifstream fin(argv[1], ifstream::binary);
  if (! fin.good())
    throw Error("Could not open local file.");

  fin.seekg(0, ios_base::end);
  const size_t n = fin.tellg();

  ios_base::openmode mode = ios_base::out;
  if (argc > 3)
    {
      if (string(argv[3]).find("app") != string::npos)
        mode = mode | ios_base::app;
      if (string(argv[3]).find("trunc") != string::npos)
        mode = mode | ios_base::trunc;
    }

  size_t off(0);
  if (argc > 4)
    {
      stringstream ss(argv[4]);
      ss >> off;
    }

  osfstream<sftp> fout(argv[2], mode);
  if (! fout.good())
    throw Error("Could not open file.");

  if (off > 0)
    fout.seekp(off, ios_base::beg);

  {
    fout << string(100, '=') << endl;
    fin.seekg(0, ios_base::beg);
    vector<char> buffer(n, '\0');
    fin.read(&buffer[0], n * sizeof(char));
    fout.write(&buffer[0], n * sizeof(char));
  }

  {
    fout << endl << string(100, '=') << endl;
    fin.seekg(0, ios_base::beg);
    string word;
    while (fin >> word)
      fout << " " << word;
  }

  {
    fout << endl << string(100, '=') << endl;
    fin.clear();
    fin.seekg(0, ios_base::beg);
    string line;
    while (getline(fin, line))
      fout << " " << line;
    fout << endl << string(100, '=') << endl;
  }

  fout.clear();
  fout.close();

  if (! fout.good())
    throw Error("Could not close file.");    

  fin.close();

  END;

  return 0;
}
