// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

int main(int argc, char *argv[])
{
  TRY;

  if (argc < 2)
    throw string("Need at least two arguments [logname@]hostname:[port] [/]path/to/file1 [[/]path/to/file2]");

  isfstream<sftp> fin;

  sfilebuf<sftp> *buf = fin.rdbuf();

  buf->trace(LIBSSH2_TRACE_SCP | LIBSSH2_TRACE_SFTP | LIBSSH2_TRACE_AUTH |
             LIBSSH2_TRACE_PUBLICKEY | LIBSSH2_TRACE_KEX | LIBSSH2_TRACE_CONN |
             LIBSSH2_TRACE_TRANS | LIBSSH2_TRACE_SOCKET | LIBSSH2_TRACE_ERROR);

  DISP(argv[1]);
  buf->connect(string(argv[1]));

  for (int i = 2; i < argc; ++i)
    {
      cout << string(100, '=') << endl;
      DISP(i);
      DISP(argv[i]);

      fin.open(argv[i]);
      if (! fin.good())
        throw Error("Unable to open file \"" + string(argv[i]) + "\".");

      const size_t n = buf->file()->size();
      DISP(n);

      vector<char> buffer(n, '\0');
      fin.read(buffer.data(), n * sizeof(char));

      for (int j = 0; j < n; ++j)
        cout << buffer[j];
      cout << endl;

      fin.close();
    }

  cout << string(100, '=') << endl;

  buf->disconnect();

  END;

  return 0;
}
