// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SFILEBUF_HXX

#define PUT_BACK       32
#define BUFFER_SIZE  1024

namespace std
{
  template<class protocol_type>
  class sfilebuf : public protocol_type::session_type, public streambuf
  {
  public:

    typedef typename protocol_type::file_type         file_type;
    typedef typename protocol_type::session_type      session_type;
    typedef typename file_type::libssh2_session_type  libssh2_session_type;

  private:

    /*!< Where to put back.*/
    const size_t put_back_;

    /*!< Buffer size.*/
    const size_t buffer_size_;

    /*!< Internal buffer.*/
    char* buffer_;

    /*!< External buffer.*/
    file_type file_;

    /*!< Open mode.*/
    ios_base::openmode mode_;

    /*!< Reading or writing.*/
    bool reading_, writing_;

    /*!< What happen when buffer underflows.*/
    virtual streambuf::int_type underflow();

    /*!< What happen when buffer overflows.*/
    virtual streambuf::int_type overflow(int_type ch = traits_type::eof());

    /*!< Synchronize with target.*/
    virtual int sync();

    /*!< Get and set position.*/
    streambuf::pos_type seekpos(streambuf::pos_type pos, ios_base::openmode which);
    streambuf::pos_type seekoff(streambuf::off_type off, ios_base::seekdir way, ios_base::openmode which);

  public:

    /*!< Constructor.*/
    sfilebuf();

    /*!< Destructor.*/
    ~sfilebuf();

    /*!< Is file opened ? */
    bool is_open() const;

    /*!< Open.*/
    sfilebuf<protocol_type>* open(const char *cpath, ios_base::openmode mode,
                                  option::base *opt1 = NULL, option::base *opt2 = NULL,
                                  option::base *opt3 = NULL);

    /*!< Connect.*/
    void connect(string address, int libssh2_debug_level = -1);

    /*!< Close.*/
    sfilebuf<protocol_type>* close();

    /*!< Get external buffer.*/
    file_type* file();
  };
}

#define SFSTREAM_FILE_SFILEBUF_HXX
#endif
