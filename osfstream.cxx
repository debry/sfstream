// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_OSFSTREAM_CXX

namespace std
{
  // Constructors.
  template<class protocol_type>
  osfstream<protocol_type>::osfstream() : std::ostream(), buffer_()
  {
    init(&buffer_);
    return;
  }

  template<class protocol_type>
  osfstream<protocol_type>::osfstream(const char *cpath, ios_base::openmode mode,
                                      option::base *opt1, option::base *opt2, option::base *opt3)
    : std::ostream(), buffer_()
  {
    init(&buffer_);
    open(cpath, mode | ios_base::out, opt1, opt2, opt3);
    return;
  }


  // Destructor.
  template<class protocol_type>
  osfstream<protocol_type>::~osfstream()
  {
    buffer_.close();
    return;
  }


  // Get methods.
  template<class protocol_type>
  sfilebuf<protocol_type>* osfstream<protocol_type>::rdbuf()
  {
    return &buffer_;
  }


  // Open.
  template<class protocol_type>
  void osfstream<protocol_type>::open(const char *cpath, ios_base::openmode mode,
                                      option::base *opt1, option::base *opt2, option::base *opt3)
  {
    if (! buffer_.open(cpath, mode | ios_base::out, opt1, opt2, opt3))
      this->setstate(ios_base::failbit);
    else
      this->clear();
  }


  // Close.
  template<class protocol_type>
  void osfstream<protocol_type>::close()
  {
    if (! buffer_.close())
      this->setstate(ios_base::failbit);
  }
}
#define SFSTREAM_FILE_OSFSTREAM_CXX
#endif
