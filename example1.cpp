// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

#ifndef PROTOCOL
#define PROTOCOL scp
#endif

int main(int argc, char *argv[])
{
  TRY;

  chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();

  if (argc == 1)
    throw string("Need one argument [[logname@]hostname:[port:]][/]path/to/file");

  isfstream<PROTOCOL> fin(argv[1], ios_base::in);

  if (! fin.good())
    throw Error("Could not open file.");

  const int n = fin.rdbuf()->file()->size();
  DISP(n);

  cout << string(100, '=') << endl;

  {
    vector<char> buffer(n, '\0');

    fin.read(&buffer[0], n * sizeof(char));

    for (int i = 0; i < n; ++i)
      cout << buffer[i];

    fin.close();

    if (! fin.good())
      throw Error("Could not close file.");
  }

  cout << string(100, '=') << endl;

  {
    fin.open(argv[1], ios_base::in);
    if (! fin.good())
      throw Error("Could not open file.");

    string buffer;
    while(fin >> buffer)
      cout << " " << buffer;
    cout << endl;

    fin.clear();
    fin.close();
    if (! fin.good())
      throw Error("Could not close file.");
  }

  cout << string(100, '=') << endl;

  {
    fin.open(argv[1]);
    if (! fin.good())
      throw Error("Could not open file.");

    string line;
    while(getline(fin, line))
      cout << " " << line;
    cout << endl;

    fin.clear();
    fin.close();

    if (! fin.good())
      throw Error("Could not close file.");
  }

  cout << string(100, '=') << endl;

  chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();

  cout << "wall time = " << chrono::duration<double, std::milli>(end - start).count() << " ms." << endl;

  END;

  return 0;
}
