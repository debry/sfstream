// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SFILEBUF_CXX

namespace std
{
  // Constructor.
  template<class protocol_type>
  sfilebuf<protocol_type>::sfilebuf()
    : session_type(), std::streambuf(), put_back_(PUT_BACK),
      buffer_size_(BUFFER_SIZE + put_back_),
      buffer_(NULL), file_(), reading_(false),
      mode_(ios_base::openmode(0)), writing_(false)
  {
    return;
  }


  // Destructor.
  template<class protocol_type>
  sfilebuf<protocol_type>::~sfilebuf()
  {
    this->close();
    return;
  }


  // Is file opened ?
  template<class protocol_type>
  bool sfilebuf<protocol_type>::is_open() const
  {
    return file_.is_open();
  }


  // Open.
  template<class protocol_type>
  sfilebuf<protocol_type>* sfilebuf<protocol_type>::open(const char *cpath, ios_base::openmode mode,
                                                         option::base *opt1, option::base *opt2,
                                                         option::base *opt3)
  {
    size_t size(-1);
    mode_t perm(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    int libssh2_debug_level(-1);

    option::vector voption = {opt1, opt2, opt3};
    voption.get("size", size);
    voption.get("perm", perm);
    voption.get("libssh2_debug_level", libssh2_debug_level);

    sfilebuf<protocol_type> *ret = NULL;

    string address, path(cpath);

    // Parse input string for address and file path.
    size_t pos;

    if ((pos = path.find_last_of(':')) != string::npos)
      {
        address = path.substr(0, pos);
        path = path.substr(pos + 1);
      }

    session::base<libssh2_session_type>::connect(address, libssh2_debug_level);

    if (this->session_ptr_ != NULL)
      if (file_.open(path.c_str(), mode, perm, size, this->session_ptr_))
        {
          mode_ = mode;
          reading_ = false;
          writing_ = false;

          if (buffer_size_ > 0)
            {
              buffer_ = new char[buffer_size_];
              if (buffer_ == NULL)
                return ret;

              bzero(buffer_, buffer_size_);

              char *end = buffer_ + buffer_size_;
              setg(end, end, end);

              char *base = buffer_;
              setp(base, base + buffer_size_ - 1);
            }
          else
            {
              setg(NULL, NULL, NULL);
              setp(NULL, NULL);
            }

          ret = this;
        }

    return ret;
  }


  // Connect.
  template<class protocol_type>
  void sfilebuf<protocol_type>::connect(string address, int libssh2_debug_level)
  {
    session::base<libssh2_session_type>::connect(address, libssh2_debug_level);
  }


  // Close.
  template<class protocol_type>
  sfilebuf<protocol_type>* sfilebuf<protocol_type>::close()
  {
    sfilebuf<protocol_type> *ret = NULL;

    if (file_.is_open())
      {
        sync();

        reading_ = false;
        writing_ = false;
        mode_ = ios_base::openmode(0);

        if (buffer_ != NULL)
          {
            delete[] buffer_;
            buffer_ = NULL;
          }

        setg(NULL, NULL, NULL);
        setp(NULL, NULL);

        if (file_.close())
          ret = this;
      }

    return ret;
  }


  // What happen when buffer underflows.
  template<class protocol_type>
  streambuf::int_type sfilebuf<protocol_type>::underflow()
  {
    const bool testin = mode_ & ios_base::in;
    int_type ret = traits_type::eof();

    if (testin && ! writing_)
      {
        // Buffer not exhausted.
        if (gptr() < egptr())
          return traits_type::to_int_type(*gptr());

        if (buffer_ != NULL)
          {
            char *base = buffer_;
            char *start = base;

            // True when this isn't the first fill.
            if (eback() == base)
              {
                // Make arrangements for putback characters.
                std::memmove(base, egptr() - put_back_, put_back_);
                start += put_back_;
              }

            // Start is now the start of the buffer, proper.
            size_t count = buffer_size_ - (start - base);

            // Get characters from file into buffer.
            size_t n = file_.xsgetn(start, count);

            if (n == 0)
              return traits_type::eof();

            // Set buffer pointers.
            setg(base, start, start + n);

            ret = traits_type::to_int_type(*gptr());
          }
        else
          {
            char ch;
            file_.xsgetn(&ch, 1);
            ret = traits_type::to_int_type(ch);
          }
      }

    return ret;
  }


  // What happen when buffer overflows.
  template<class protocol_type>
  streambuf::int_type sfilebuf<protocol_type>::overflow(int_type ch)
  {
    int_type ret = traits_type::eof();
    const bool testeof = traits_type::eq_int_type(ch, ret);
    const bool testout = mode_ & ios_base::out;

    if (testout && ! reading_)
      {
        if (this->pbase() < this->pptr())
          {
            // If appropriate, append the overflow char.
            if (!testeof)
              {
                *this->pptr() = traits_type::to_char_type(ch);
                this->pbump(1);
              }

            // Output.
            if (file_.xsputn(reinterpret_cast<char*>(this->pbase()), this->pptr() - this->pbase()))
              {
                std::ptrdiff_t n = this->pptr() - this->pbase();
                this->pbump(-n);
                ret = traits_type::not_eof(ch);
              }
          }
        else if (buffer_size_ > 1)
          {
            if (!testeof)
              {
                *this->pptr() = traits_type::to_char_type(ch);
                this->pbump(1);
              }
            ret = traits_type::not_eof(ch);
          }
        else
          {
            // Unbuffered.
            char_type conv = traits_type::to_char_type(ch);
            if (testeof || file_.xsputn(&conv, 1))
              ret = traits_type::not_eof(ch);
          }
      }

    return ret;
  }


  // Synchronize with target.
  template<class protocol_type>
  int sfilebuf<protocol_type>::sync()
  {
    // Make sure that the internal buffer resyncs its
    // idea of the file position with the external file.
    int ret = 0;

    if (this->pbase() < this->pptr())
      {
        const int_type tmp = this->overflow();
        if (traits_type::eq_int_type(tmp, traits_type::eof()))
          ret = -1;
      }

    return ret;
  }


  // Get and set position.
  template<class protocol_type>
  streambuf::pos_type sfilebuf<protocol_type>::seekpos(streambuf::pos_type pos, ios_base::openmode which)
  {
    pos_type ret =  pos_type(off_type(-1));

    if (file_.is_open())
      {
        ret = pos_type(file_.seekoff(off_type(pos), ios_base::beg));
        //if (ret != pos_type(off_type(-1)))
        //  setg(buffer_, buffer_, buffer_);
      }

    return ret;
  }


  template<class protocol_type>
  streambuf::pos_type sfilebuf<protocol_type>::seekoff(streambuf::off_type off,
                                                       ios_base::seekdir way,
                                                       ios_base::openmode which)
  {
    pos_type ret =  pos_type(off_type(-1));

    const int width = 1;
    const bool testfail = off != 0 && width <= 0;

    off_type computed_off = off * width;

    if (file_.is_open() && ! testfail)
      {
        if (way == ios_base::cur)
          computed_off += gptr() - egptr();

        ret = pos_type(file_.seekoff(computed_off, way));
        //if (ret != pos_type(off_type(-1)))
        //  setg(buffer_, buffer_, buffer_);
      }

    return ret;
  }


  // Get external buffer.
  template<class protocol_type>
  typename protocol_type::file_type* sfilebuf<protocol_type>::file()
  {
    return &file_;
  }
}

#define SFSTREAM_FILE_SFILEBUF_CXX
#endif
