// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

int main(int argc, char *argv[])
{
  TRY;

  if (argc < 3)
    throw string("Need two arguments /path/to/local/file [logname@]hostname:[port:][/]path/to/file [file_size]");

  ifstream fin(argv[1], ifstream::binary);
  if (! fin.good())
    throw Error("Could not open local file.");

  fin.seekg(0, ios_base::end);
  const size_t local_file_size = fin.tellg();
  fin.seekg(0, ios_base::beg);
  DISP(local_file_size);

  size_t remote_file_size(0);
  if (argc > 3)
    {
      istringstream iss(argv[3]);
      iss >> remote_file_size;
    }

  if (remote_file_size == 0)
    remote_file_size = local_file_size;
  DISP(remote_file_size);

  osfstream<scp> fout(argv[2], ios_base::out, option::size = remote_file_size);
  if (! fout.good())
    throw Error("Could not open file.");

  cout << string(100, '=') << endl;

  const int n = local_file_size; //remote_file_size > local_file_size ? local_file_size : remote_file_size;
  DISP(n);
  vector<char> buffer(n, '\0');

  fin.read(&buffer[0], n * sizeof(char));
  for (int i = 0; i < n; ++i)
    cout << buffer[i];

  fout.write(&buffer[0], n * sizeof(char));

  //fout.flush();
  //fout.rdbuf()->file()->fill(' ');

  fout.clear();
  fout.close();

  if (! fout.good())
    throw Error("Could not close file.");    

  cout << string(100, '=') << endl << endl;

  END;

  return 0;
}
