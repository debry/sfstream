// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SFSTREAM_FILE_SFSTREAM_HEADER_HXX

namespace std
{
}

#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <exception>
#include <streambuf>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Network.
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

// SSH2 library.
#include <libssh2.h>
#include <libssh2_sftp.h>

#ifndef DISP
#define DISP(x) cout << #x ": " << x << endl
#endif
#ifndef ERR
#define ERR(x) cout << "Hermes - " #x << endl
#endif
#ifndef CBUG
#define CBUG cout << __FILE__ << ":" <<__FUNCTION__ << ":" << __LINE__ << ":"
#endif

namespace std
{
  template<typename T>
  string to_str(const T& value);

  template<typename T>
  T convert(const char* s);
}

#include "error.hxx"
#include "session.hxx"
#include "basic_file.hxx"
#include "protocol.hxx"
#include "option.hxx"
#include "sfilebuf.hxx"
#include "isfstream.hxx"
#include "osfstream.hxx"

#ifdef TRY
#undef TRY
#endif
#define TRY try                                         \
    {

#ifdef END
#undef END
#endif
#define END                                             \
  }                                                     \
    catch (std::Error &err)                             \
      {                                                 \
        err.CoutWhat();                                 \
        return 1;                                       \
      }                                                 \
    catch (std::libssh2::Error &err)                    \
      {                                                 \
        err.CoutWhat();                                 \
        return 1;                                       \
      }                                                 \
    catch (std::exception& err)                         \
      {                                                 \
        cout << "C++ exception: "                       \
             << err.what() << endl;                     \
        return 1;                                       \
      }                                                 \
    catch (std::string& str)                            \
      {                                                 \
        cout << str << endl;                            \
        return 1;                                       \
      }                                                 \
    catch (const char* str)                             \
      {                                                 \
        cout << str << endl;                            \
        return 1;                                       \
      }                                                 \
    catch(...)                                          \
      {                                                 \
        cout << "Unknown exception..." <<endl;          \
        return 1;                                       \
      }

#define SFSTREAM_FILE_SFSTREAM_HEADER_HXX
#endif
