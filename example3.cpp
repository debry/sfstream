// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of sfstream.
//
// sfstream is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// sfstream is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with sfstream.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "sfstream.hxx"
using namespace protocol;

#ifndef PROTOCOL
#define PROTOCOL sftp
#endif

int main(int argc, char *argv[])
{
  TRY;

  if (argc == 1)
    throw string("Need one argument [[logname@]hostname:[port:]][/]path/to/file");

  isfstream<PROTOCOL> fin(argv[1]);

  if (! fin.good())
    throw Error("Could not open file.");

  const int n = fin.rdbuf()->file()->size();
  DISP(n);

  cout << string(100, '=') << endl;

  {
    const int p = n;
    vector<char> buffer(p, '\0');

    fin.read(&buffer[0], p * sizeof(char));

    for (int i = 0; i < p; ++i)
      cout << buffer[i];

    DISP(fin.tellg());
  }

  cout << string(100, '=') << endl;

  {
    const int p = n / 2;
    fin.seekg(p);
    DISP(fin.tellg());

    vector<char> buffer(p, '\0');

    fin.read(&buffer[0], p * sizeof(char));

    for (int i = 0; i < p; ++i)
      cout << buffer[i];

    DISP(fin.tellg());
  }

  cout << string(100, '=') << endl;

  {
    const int p = n / 3;
    fin.seekg(- p, ios_base::end);
    DISP(fin.tellg());

    vector<char> buffer(p, '\0');

    fin.read(&buffer[0], p * sizeof(char));

    for (int i = 0; i < p; ++i)
      cout << buffer[i];

    DISP(fin.tellg());
  }

  cout << string(100, '=') << endl << endl;

  END;

  return 0;
}
